import requests
import itertools
import time
import copy
import pynder
import urllib
import cStringIO
import json
import re

#########################
#       constants       #
#########################

gm_token = "WxcVQ0YasJ8HyFCtj5jA6h47NuIWTNbRqtqoemoZ"
gm_chat_url = "https://api.groupme.com/v3/groups/40983265/messages"
bot_id = "e020e740685485f7044f113927"
fb_token = "EAAGm0PX4ZCpsBAN1o2hQ9CcHrTkFpcIuq7JQvR1ilYtw3ismrghW3ZAG1G1wmSnYXU3P6GSfWUsDuFr4HVVIZA4dOseGD5q3zADwP2FZBEgvr0w84iFjVVMZCWcXiSwxvWpi5k8JGQIQkbdsMeuYsoJWaNC1ZA0YrEMXcDaZBr8ZB0QOOYEMRaAOadV25ZAkNnAjMXfZBtRfQE23Ukcw4bMOjKydT0qnVD71yizUA7GJXB5VVjHGJaTu7ZCyYPCOdrkA1IZD"
fb_id = "100002846211575"
latitude = 41.975853
longitude = -91.670410

##############################
#       state variables      #
##############################

current_user = None
displayed_user = None
photo_inex = 0

# Start the session and load users
session = pynder.Session(facebook_id=fb_id, facebook_token=fb_token)
session.update_location(latitude, longitude)
users = iter(session.nearby_users())

#################################
#       utility functions       #
#################################

# Continually checks for new messages, applying a function to them. Exit if the funcion returns False
def input_loop(func, interval)
    # Grab messages that are already in the chat
    old_messages = get_messages()
    
    result = True 
    # Main program loop
    while result:
        # Get the 20 most recent messages
        response_messages = get_messages()
    
        # Process only the new messages
        new_messages = []
        if response_messages != old_messages:
            new_messages = copy.copy(response_messages)
            # Remove all old messages
            for m in old_messages:
                if m in new_messages:
                    new_messages.remove(m)
            
            # Process the new messages
            for message in new_messages:
                if message['name'] != 'TinderTender':
                    result, return_value = func(message)
    
        old_messages = copy.copy(response_messages)
        
        time.sleep(interval)
        
    return return_value
        
# Posts a message to the chat
def say(message):
    post_params = { 'bot_id' : bot_id, 'text': message }
    requests.post('https://api.groupme.com/v3/bots/post', params = post_params)

# Posts an image message to the chat
def post_photo(url, caption, size='large'):
    # Load image data from tinder url
    file = cStringIO.StringIO(urllib.urlopen(url).read())
    
    try:
        # Upload image to groupme
        post_payload = {'data': file}
        post_params = {'access_token': gm_token}
        post_headers = {'content-type': 'image/jpeg', 'x-access_token': gm_token}
        response = requests.post('https://image.groupme.com/pictures', json=payload, params=post_params, headers=post_headers)
        gm_url = response['payload']['url'] + '.' + size
        
        # Post the image using the bot
        post_params = { 'bot_id' : bot_id, 'text': caption, 'attachments': ['type': 'image', 'url': gm_url] }
        requests.post('https://api.groupme.com/v3/bots/post', params = post_params)
    except Exception as e:
        say('failed to post image')
        print(e)
    
# Checks if supplied args are valid for a command
def validate_args(args, patterns):
    if len(patterns) != args:
        say('Usage error: wrong number of arguments supplied. Type "help" for usage.')
        return False
        
    for pattern, arg in zip(patterns, args):
        r = re.compile(pattern)
        if not(r.match(arg)):
            say('Usage error: argument "' + arg + '" did not match expected format. Type "help" for usage.')
            return False
    
    return True
    
# Grabs the top 20 latest messages from groupme via http request
def get_messages():
    m = requests.get(gm_chat_url, params = {'token': gm_token}).json()['response']['messages']
    m.reverse;
    return m
    
# Returns a match object for a given name. If multiple such matches exist, lets the user specify based on profile pic
def find_match(name):
    matches = [m for m in session.matches() if m.user is not None and m.user.name == name]
    
    if len(matches) == 0:
        say('no match found with name ' + name)
        return False
    elif len(matches) == 1:
        return matches[0]
    elif len(matches) > 1:
        say("Multiple matches found named " + name + ". Please select one:")
        
        # Ask user to clarify which match he wants
        options = []
        for i,m in enumerate(matches):
            p = m.user.get_photos()[0]
            post_photo(p, i+1, size='preview')
            options.append(str(i+1))
                
        def select_num_curry(options):
            def select_num(message):
                    if message['text'] in options:
                        return False, options.index(message['text'])
                    else:
                        say('Please type one of the following: ' + ", ".join(options))
                        return True
                    
            return select_num
                
        # Prompt the user for input
        selected_index = input_loop(select_num_curry(options), 5)
         
        return matches[selected_index]
        
# Displays the name, age, bio, and photo for a given user
def display_user(user):
    photos = user.get_photos()
    msg = user.name + ', ' + user.age +' (' + str(photo_index + 1) + '/' + str(len(photos)) + ')\n'\
          user.distance_mi + ' miles away\n'\
          user.bio
    post_photo(photos[photo_index], msg)
    displayed_user = user
    
# Moves to the next user on the stack
def next_user():
    try: 
        current_user = next(users)
        photo_index = 0
        display_user(current_user)
    else:
        current_user = None;
        displayed_user = None;
        say('There are no more users in your area! You can try typing refresh')

# Convert seconds into something easily human readable        
def readable_time(seconds):
    if seconds < 60:
        return str(seconds) + " seconds"
    elif seconds < 3600:
        minutes = int(seconds/60))
        seconds = seconds - minutes*60
        return str(minutes) + " minutes and " + seconds + " seconds"
    else:
        hours = int(seconds/3600)
        minutes = int((seconds - hours*3600) / 60)
        seconds = seconds - hours*3600 - minutes*60
        return str(hours) + " hours, " + str(minutes) + " minutes and " + str(seconds) + " seconds"
    
next_user()

#############################
#       user commands       #
#############################

# list n most recent matches
# args: [count]
def matches(args):
    if not(validate_args(args, ['\d*']):
        return
        
    msg = ''
    count = args[0]
    for i, m in enumerate(session.matches()):
        if i >= count:
            break
            
        msg += m.user.name + '\n'
    say(msg)

# Display bio and photos for a match
# args: [name]
def info(args):
    if not(validate_args(args, ['.*']):
        return
    
    name = args[0]
    match = find_match(name)
    
    if not(match):
        return
        
    display_user(match.user)

# reply to the latest incoming message
# args: [message]
def reply(args):
    if not(validate_args(" ".join(args), ['.*']):
        return
    
    message = " ".join(args)
    match = session.matches[0]
    
    if not(match):
        return
    
    if match.message(message)['status_code'] == 200
        say('Message sent to ' + match.user.name ".")
    else:
        say('Message failed to send.')

# display the last n messages with name
# args: [name, count]
def convo(args):
    if not(validate_args(args, ['.*', '\d*']):
        return
    
    name = args[0]
    count = int(args[1])
    match = find_match(name)
    
    if not(match):
        return
    
    msg = ''
    
    for i, m in enumerate(match.messages):
        if i >= count:
            break
        
        msg += m['name'] + ': ' + ['text'] + '\n'
    say(msg)
        
# display the last n messages with name        
# args: [name, message]
def send(args):
    if not(validate_args([args[0], " ".join(args[1:])], ['.*', '.*']):
        return
        
    name = args[0]
    message = " ".join(args[1:])
    match = find_match(name)
    
    if not(match):
        return
    
    if match.message(message)['status_code'] == 200
        say('Message sent to ' + match.user.name ".")
    else:
        say('Message failed to send.')

# diplay the name, profile, and bio of the top of the deck    
# args: []
def current(args):
    if not(validate_args(args, []):
        return
        
    if current_user == None:
        say('no user to display')
        
    display_user(current_user)

# diplay the next image in the current profile    
# args: []
def next(args):
    if not(validate_args(args, []):
        return
        
    photos = displayed_user.get_photos()
    if (photo_index + 1 < len(photos)):
        photo_index += 1
        post_photo(photos[photo_index], '(' + str(photo_index + 1) + '/' + str(len(photos)) + ')')
    else:
        say('That was the last photo')

# diplay the previous image in the current profile   
# args: []
def prev(args):
    if not(validate_args(args, []):
        return
        
    photos = displayed_user.get_photos()
    if (photo_index > 0):
        photo_index -= 1
        post_photo(photos[photo_index], '(' + str(photo_index + 1) + '/' + str(len(photos)) + ')')
    else:
        say('That was the first photo')

# like the current profile   
# args: []
def right(args):
    if not(validate_args(args, []):
        return
        
    if session.likes_remaining == 0:
        say("You don't have any likes left!\nTry again in " + readable_time(session.can_like_in) + ".")
        return
        
    say('Liked ' user.name)
    if user.like():
        say("It's a match!")
        
    next_user()
    
# dislike the current profile   
# args: []
def left(args):
    if not(validate_args(args, []):
        return
        
    say('Disliked ' user.name)
    user.dislike()
    
    next_user()

# superlike the current profile       
# args: []
def up(args):
    if not(validate_args(args, []):
        return
        
    if session.super_likes_remaining == 0:
        say("You don't have any super likes left!")
        return
        
    say('Superliked ' user.name)
    if user.superlike():
        say("It's a match!")
        
    next_user()

# skip the current profile       
# args: []
def skip(args):
    if not(validate_args(args, []):
        return
        
    users.append(current_user)
    
    next_user()

# skip the current profile       
# args: []
def likes(args):
    if not(validate_args(args, []):
        return
        
    say('You have ' + str(session.likes_remaining) ' like(s) remaining.')
    
# skip the current profile       
# args: []
def superlikes(args):
    if not(validate_args(args, []):
        return
        
    say('You have ' + str(session.super_likes_remaining) ' super like(s) remaining.')
    
# show how many likes you have left       
# args: []
def refresh(args):
    if not(validate_args(args, []):
        return
        
    users = iter(session.nearby_users())
    
    next_user()
    
# set location of to look for users
# args: [latitude, longitude]
def set_location(args):
    if not(validate_args(args, ['[\d\.]*','[\d\.]*']):
        return
        
    session.set_location(args[0], args[1])
    say('Successfully set location.')
    refresh()

# display the help text   
# args: []
def _help(args):
    if not(validate_args(args, []):
        return
        
    msg = 'Here is the list of commands:\n'\
          'matches <count>: list n most recent matches\n'\
          'info <name>: Display bio and photos for a match\n'\
          'reply <message>: reply to the latest incoming message\n'\
          'convo <name> <count>: display the last n messages with name\n'\
          'send <name> <message>: send a message\n'\
          'current: diplay the name, profile, and bio of the top of the deck\n'\
          'next: diplay the next image in the current profile\n'\
          'prev: diplay the previous image in the current profile\n'\
          'right: like the current profile\n'\
          'left: dislike the current profile\n'\
          'up: superlike the current profile\n'\
          'skip: go to the next profile on the stack without acting on it\n'\
          'likes: show how many likes you have left\n'\
          'superlikes: show how many super likes you have left\n'\
          'refresh: reload the list of nearby users'
    say(msg)

# Dictionary for mapping commands onto their functions
commands = {
        "matches": matches,
        "info": info,
        "reply": reply,
        "convo": convo,
        "send": send,
        "current": current,
        "next": next,
        "prev": prev,
        "left": left,
        "right": right,
        "up": up,
        "skip": skip,
        "likes": likes,
        "superlikes": superlikes,
        "refresh": refresh,
        "help": _help
}

#########################
#       main loop       #
#########################

if __name__ == "__main__":

    # Process a message and do something
    def process_message(message):
        tokens = message['text'].split()
    
        try:
            commands[tokens[0]](tokens[1:])
            return True, ''
        except:
            say("Sorry, I didn't recognize that command. Type 'help' for a list of commands")
            return True, ''
        
    input_loop(process_message, 5)
    

